package chap03.array;

import java.util.Arrays;

public class ArrayClass {

	public static void main(String[] args) {
		study004();
		//study003();
		//study002();
		//study001();
	}
	
	// Xắp xếp
	public static void study004() {
		int arrInt[] = {30, 31, 24, 36, 72};
		System.out.println("Input: " + Arrays.toString(arrInt));
		
		// Xắp xếp tăng dần
		Arrays.sort(arrInt);
		System.out.println("Output: " + Arrays.toString(arrInt));
		
		// Xắp xếp giảm dần: Xếp tăng dần sau đó đảo ngược mảng
		int length = arrInt.length;
		for (int i = 0; i < length / 2; i++) {
			// Đảo vị trí arrInt[i]
			// cho arrInt[length - 1 - i]
			// sử dụng biến trung gian temp
			int temp = arrInt[i];
			arrInt[i] = arrInt[length - 1 - i];
			arrInt[length - 1 - i] = temp;
		}
		
		System.out.println("Output: " + Arrays.toString(arrInt));
	}
	
	// In mảng
	public static void study003() {
		String arrStr[] = {"Java", "IOS", "Android", "C#", "Ruby"};
		
		System.out.println("------------------------------------");
		int length = arrStr.length;
		// sử dụng vòng lặp for
		for (int i = 0; i < length; i++) {
			System.out.printf("Phần tử thứ %d: %s %n", i, arrStr[i]);
		}
		
		System.out.println("------------------------------------");
		// sử dụng for in
		for (String str : arrStr) {
			System.out.println(str);
		}
		
		System.out.println("------------------------------------");
		// sử dụng toString
		System.out.println(Arrays.toString(arrStr));
	}
	
	// Sao chép mảng có điều kiện sử dụng: copyOfRange
	public static void study002() {
		String arrStr[] = {"Java", "PHP", "C#", "Javascript", "MySQL"};
		
		String newArr[] = Arrays.copyOfRange(arrStr, 2, 4);
		
		System.out.println(newArr.length);
		System.out.println(newArr[1]);
	}
	
	// Sao chép mảng sử dụng: copyOf
	public static void study001() {
		String arrStr[] = {"Java", "PHP", "C#", "Javascript", "MySQL"};
		
		String newArr[] = Arrays.copyOf(arrStr, 7);
		newArr[5] = "Android";
		newArr[6] = "IOS";
		System.out.println(newArr[5]);
		System.out.println(newArr[6]);
	}
}