package chap03.array;

import java.util.Arrays;

public class ArrayMulti {

	public static void main(String[] args) {
		study002();
		//study001();
	}
	
	public static void study002() {
		// 2 9 1
		// 3 6 7
		int arrMulti[][] = {{2, 9, 1}, {3, 6, 7}};
		
		// Hàng thứ 1 row = 0
		arrMulti[0][0] = 2;
		arrMulti[0][1] = 9;
		arrMulti[0][2] = 1;
		
		// Hàng thứ 2 row = 1
		arrMulti[1][0] = 3;
		arrMulti[1][1] = 6;
		arrMulti[1][2] = 7;
		
		System.out.println("Dòng 2 cột 1: " + arrMulti[1][0]);
		
		System.out.println("Số dòng: " + arrMulti.length);
	}
	
	public static void study001() {
		// 2 9 1
		// 3 6 7
		int arrMulti[][] = new int[2][3];
		
		// Hàng thứ 1 row = 0
		arrMulti[0][0] = 2;
		arrMulti[0][1] = 9;
		arrMulti[0][2] = 1;
		
		// Hàng thứ 2 row = 1
		arrMulti[1][0] = 3;
		arrMulti[1][1] = 6;
		arrMulti[1][2] = 7;
		
		System.out.println("Dòng 2 cột 1: " + arrMulti[1][0]);
		
		System.out.println("Số dòng: " + arrMulti.length);
	}

}
