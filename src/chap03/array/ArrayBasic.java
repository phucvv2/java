package chap03.array;

import java.util.Scanner;

public class ArrayBasic {
	public static void main(String[] args) {
		study004();
		//study003();
		//study002();
		//study001();
	}
	
	public static void study004() {
		int[] arrInt = {1, 2, 4, 8, 0, -2, 99};
		
		int sum = 0;
		int min = arrInt[0];
		int max = arrInt[0];
		
		for (int number : arrInt) {
			sum = sum + number;
			
			if (min > number) min = number;
			if (max < number) max = number;
		}
		
		System.out.printf("Tổng số phần tử là: %d %n", sum);
		System.out.printf("Phần tử nhỏ nhất là: %d %n", min);
		System.out.printf("Phần tử lớn nhất là: %d %n", max);
	}
	
	public static void study003() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Length: ");
		
		int length = sc.nextInt();
		sc.nextLine();
		
		int[] arrInt = new int[length];
		
		for (int i = 0; i < length; i++) {
			System.out.printf("Nhập vào phần tử thứ %d: %n", i);
			arrInt[i] = sc.nextInt(); 
			sc.nextLine();
		}
		
		for (int i = 0; i < arrInt.length; i++) {
			System.out.printf("Phần tử thứ %d: là %d %n", i, arrInt[i]);
		}
		
		sc.close();
	}
	
	public static void study002() {
		int[] arr = {30, 31, 32, 33, 34, 35, 36, 37};
		
		System.out.println("Phần tử thứ 1: " + arr[0]);
		System.out.println("Phần tử thứ 2: " + arr[1]);
		System.out.println("Phần tử thứ 3: " + arr[2]);
		System.out.println("Phần tử thứ 4: " + arr[3]);
		System.out.println("Phần tử thứ 5: " + arr[4]);
		
		System.out.println("=====================================");
		
		System.out.println("Tổng số phần tử: " + arr.length);
		System.out.println("Phần tử đầu tiên: " + arr[0]);
		System.out.println("Phần tử ở giữa: " + arr[(arr.length - 1 ) / 2]);
		System.out.println("Phần tử cuối cùng: " + arr[arr.length - 1]);
		
		System.out.println("=====================================");
		
		int length = arr.length;
		// Cách 1:
		// Duyệt mảng sử dụng vòng lặp for
		for (int i = 0; i < length; i++) {
			System.out.println("Phần tử thứ " + i + ": " + arr[i]);
		}
		
		System.out.println("=====================================");
		
		// Cách 2:
		int j = 0;
		for (int n : arr) {
			System.out.println("Phần tử thứ " + j + ": " + n);
			j++;
		}
	}
	
	public static void study001() {
		// khai bao mang
		int arrOne[];
		int[] arrTwo;
		
		// khai bao mang va cap vung nho
		
		// Cách 1
//		int[] arrInt = new int[5];
//		arrInt[0] = 30;
//		arrInt[1] = 31;
//		arrInt[2] = 32;
//		arrInt[3] = 33;
//		arrInt[4] = 34;
		
		// Cách 2
		int[] arrInt = {30, 31, 32, 33, 34};
		
		// khai bao mang la String
		String arrStr[] = {"PHP", "Java", "Javascript", ".Net"};
		
		System.out.println("Phần tử thứ 1: " + arrInt[0]);
		System.out.println("Phần tử thứ 2: " + arrInt[1]);
		System.out.println("Phần tử thứ 3: " + arrInt[2]);
		System.out.println("Phần tử thứ 4: " + arrInt[3]);
		System.out.println("Phần tử thứ 5: " + arrInt[4]);
		
		System.out.println("===========================");
		
		System.out.println("Phần tử thứ 1: " + arrStr[0]);
		System.out.println("Phần tử thứ 2: " + arrStr[1]);
		System.out.println("Phần tử thứ 3: " + arrStr[2]);
	}
}