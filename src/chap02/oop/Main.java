package chap02.oop;

public class Main {
	// Xay dung class Fraction
	public static void main(String[] args) {
		Fraction fractionA = new Fraction(1, 4);
		Fraction fractionB = new Fraction(1, 2);
		
		Fraction fractionC = new Fraction(fractionA, fractionB, "/");
				
		
		System.out.printf("%s + %s = %s", fractionA.print(), fractionB.print(), fractionC.print());
	}
	
	//
	public static void main005(String[] args) {
		Counter counter1 = new Counter();
		Counter counter2 = new Counter();
		Counter counter3 = new Counter();
		
		Counter.showCount();
	}
	
	// Inheritance
	public static void main004(String[] args) {
		Student student = new Student("John", "SV001", 1997);
		student.name = "Test";
		student.setScore(8.5);
		student.showInfor();
	}
	// Constructor
	public static void main003(String[] args) {
		Student studentOne = new Student("Peter", "SV001", 2001);
		///////Student studentTwo = new Student("Mary", "SV002", 2001);
		//studentOne.showInfo();
	}
	
	// Overloading method ~ setCode
	public static void main002(String[] args) {
//		Student studentOne = new Student();
//		studentOne.setCode("S001");
//		System.out.println("Code: " + studentOne.getCode());
	}
	
	// getter setter
	public static void main001(String[] args) {
//		System.out.println("Main.main()");
//		
//		Student studentOne = new Student();
//		studentOne.setName("John");
//		studentOne.setCode("S001");
//		studentOne.setBirthday(1988);
//		studentOne.showInfo();
	}
}

