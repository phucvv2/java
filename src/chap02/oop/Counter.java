package chap02.oop;

public class Counter {
	public static int count = 0;
	public static String college = "CTU";
	
	public Counter() {
		count++;
	}
	
	public static int getCount() {
		return count;
	}
	
	public static void showCount() {
		System.out.println("Count: " + count);
		System.out.println("College: " + college);
		System.out.println("Get count: " + getCount());
	}
}
