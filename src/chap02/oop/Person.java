package chap02.oop;

public class Person {
	public String name;
	public String code;
	public int birthDay;
	
	public Person() {
		super();
	}
	
	public Person(String name, String code, int birthDay) {
		super();
		this.name = name;
		this.code = code;
		this.birthDay = birthDay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(int birthDay) {
		this.birthDay = birthDay;
	}
	
	public void showInfor() {
		System.out.println("Person Infor:");
		System.out.println("Name: " + this.getName());
		System.out.println("Code: " + this.getCode());
		System.out.println("Birthday: " + this.getBirthDay());
	}
}
