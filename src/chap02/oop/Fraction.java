package chap02.oop;

public class Fraction {
	private int numerator;
	private int denomirator;

	public Fraction(int numerator, int denomirator) {
		this.numerator = numerator;
		this.denomirator = denomirator;
	}

	public Fraction(Fraction fractionA, Fraction fractionB, String operator) {
		// rut gon phan so
		fractionA.normalize();
		fractionB.normalize();

		switch (operator) {
		case "+":
			this.add(fractionA, fractionB);
			break;
		case "-":
			this.sub(fractionA, fractionB);
			break;
		case "*":
		case "x":
		case "X":
		case ".":
			this.multiple(fractionA, fractionB);
			break;
		case "/":
		case ":":
			this.divide(fractionA, fractionB);
			break;

		default:
			this.add(fractionA, fractionB);
			break;
		}
	}

	// Tong 2 phan so
	// x/y + a/b = (x*b + a*y)/(y*b)
	public void add(Fraction fractionA, Fraction fractionB) {
		// set tu so
		this.setNumerator(fractionA.getNumerator() * fractionB.getDenomirator()
				+ fractionB.getNumerator() * fractionA.getDenomirator());
		// set mau so
		this.setDenomirator(fractionA.getDenomirator() * fractionB.getDenomirator());
		this.normalize();
	}

	// Hieu 2 phan so
	// x/y - a/b = (x*b - a*y)/(y*b)
	public void sub(Fraction fractionA, Fraction fractionB) {
		// set tu so
		this.setNumerator(fractionA.getNumerator() * fractionB.getDenomirator()
				- fractionB.getNumerator() * fractionA.getDenomirator());
		// set mau so
		this.setDenomirator(fractionA.getDenomirator() * fractionB.getDenomirator());
		// rut gon phan so
		this.normalize();
	}

	// Nhan 2 phan so
	// x/y * a/b = (x*a)/(y*b)
	public void multiple(Fraction fractionA, Fraction fractionB) {
		// set tu so
		this.setNumerator(fractionA.getNumerator() * fractionB.getNumerator());
		// set mau so
		this.setDenomirator(fractionA.getDenomirator() * fractionB.getDenomirator());
		// rut gon phan so
		this.normalize();
	}

	// Chia 2 phan so
	// x/y : a/b = (x*b) / (y*a)
	public void divide(Fraction fractionA, Fraction fractionB) {
		// set tu so
		this.setNumerator(fractionA.getNumerator() * fractionB.getDenomirator());
		// set mau so
		this.setDenomirator(fractionB.getNumerator() * fractionA.getDenomirator());
		// rut gon phan so
		this.normalize();
	}

	// Hien thi phan so
	public String print() {
		return this.getNumerator() + "/" + this.getDenomirator();
	}

	// Rut gon phan so
	public void normalize() {
		int ucln = this.UCNL(this.getNumerator(), this.getDenomirator());

		if (ucln > 1) {
			this.setNumerator(this.getNumerator() / ucln);
			this.setDenomirator(this.getDenomirator() / ucln);
		}
	}

	// Kiem tra phan so
	public boolean checkNormalize() {
		if (UCNL(this.getNumerator(), this.getDenomirator()) == 1)
			return true;
		return false;
	}

	// Tim UCLN
	public int UCNL(int x, int y) {
		int ucln = Math.min(x, y);

		while (ucln >= 1) {
			if (x % ucln == 0 && y % ucln == 0)
				return ucln;
			ucln--;
		}

		return ucln;
	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenomirator() {
		return denomirator;
	}

	public void setDenomirator(int denomirator) {
		this.denomirator = denomirator;
	}
}