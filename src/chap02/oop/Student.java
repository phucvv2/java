package chap02.oop;

public class Student extends Person{
	public Student(String name, String code, int birthDay) {
		super(name, code, birthDay);
	}

	public double score;

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
	
	public void showInfor() {
		super.showInfor();
		super.name = "Test";
		System.out.println("Score: " + this.getScore());
	}
}
