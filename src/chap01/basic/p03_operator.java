package chap01.basic;


public class p03_operator {
	public static void main(String[] args) {
		int max = 19;
		int min = 14;
		int range = (max - min) + 1;
		int randomNumber = 0;
		
		randomNumber = (int)(Math.random() * range) + min;
		System.out.println("Random nmber: " + randomNumber);
		
	}
	public static void main005(String[] args) {
		int numberOne = 15;
		int numberTwo = 125;
		int numberThree = 85;
		
		int result = Math.max(numberOne, numberTwo);
		int maxNumber = Math.max(result, numberThree);
		
		System.out.println("Max: " + maxNumber);
	}
	
	// Hàm toán học phổ biến
	// max: Hàm tìm số lớn nhất
	// min: Hàm tìm số nhỏ nhất
	// ceil: Hàm làm tròn lên số gần nhất và lớn nhất
	// floor: Hàm làm tròn lên số gần nhất và nhỏ nhất
	// random: Tạo số ngẫu nhiên t
	public static void main004(String[] args) {
		int numberOne = 20;
		int numberTwo = 10;
		double numberThree = 123.454;
		
		int maxNumber = Math.max(numberOne, numberTwo);
		System.out.println("Max: " + maxNumber);
		
		int minNumber = Math.min(numberOne, numberTwo);
		System.out.println("Min: " + minNumber);
		
		System.out.println("Ceil: " + Math.ceil(numberThree));
		System.out.println("Floor: " + Math.floor(numberThree));
		System.out.println("Round: " + Math.round(numberThree));
		
		double randomNumber = Math.random();
		System.out.println("Random: " + randomNumber);
	}
	
	// Toan tu ++ --
	// Tra ve gia tri sau do tang va giam di 1 don vi
	public static void main003(String[] args) {
		int i = 10;
		i++;
		System.out.println(i);
	}
	// Toan tu gan += -= *= /=
	public static void main002(String[] args) {
		int number = 20;
		number += 10;
		number -= 10;
		number *= 10;
		number %= 10;
		System.out.print(number);
	}
	
	// Toan tu so hoc + - * / %
	public static void main001(String[] args) {
		int numberOne = 20;
		int numberTwo = 3;
		int result;
		
		// + 
		result = numberOne + numberTwo;
		System.out.println(result);
		
		// - 
		result = numberOne - numberTwo;
		System.out.println(result);
		
		// * 
		result = numberOne * numberTwo;
		System.out.println(result);
		
		// / 
		result = numberOne / numberTwo;
		System.out.println(result);
		
		// % 
		result = numberOne % numberTwo;
		System.out.println(result);
	}
}
