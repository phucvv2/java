package chap01.basic;

public class p08_shape {
	// shape 08
	public static void main(String[] args) {
		/*
			   		*		1
			   	  * * *		2
			    * * * * *	3
			  * * * * * * *	4	
		 */
		// line = space + character
		// space = Height - line
		// character = 2 * line - 1
		
		final int HEIGHT = 5;
		String result = "";
		String record = "";
		String space = "";
		String character = "";
		int line = 1;
		while(line <= HEIGHT) {
			space = "";
			character = "";
			for (int s = 1; s <= HEIGHT - line; s++) {
				space += "  ";
			}
			
			for (int c = 1; c <= 2 * line - 1; c++) {
				character += "* ";
			}
			record = space + character + "\n";
			result += record;
			line++;
		}
		
		System.out.println(result);
	}
	
	// shape 07
	public static void main007(String[] args) {
		/*
		 * 	* * * * *
		 * 	*       *
		 *  *       *
		 *  *       *
		 *  * * * * *
		 */
		final int LENGHT = 9;
		int number = 1;
		while (number <= LENGHT) {
			for (int i = 1; i <= LENGHT; i++) {
				if (i >= 2 && i <= LENGHT - 1 && number >= 2 && number <= LENGHT - 1) {
					System.out.print("  ");
				} else {
					System.out.print("* ");
				}
			}
			System.out.println();
			number++;
		}
	}
	
	// shape 06
	public static void main006(String[] args) {
		/*
				  1
				2 1 2
			  3 2 1 2 3
			4 3 2 1 2 3 4
		  5	4 3 2 1 2 3 4 5
		 */
		String record = "";
		String result = "";
		String space = "";
		String left = "";
		String right = "";
		
		int number = 1;
		while(number <= 9) {
			space = "";
			left = "";
			right = "";
			
			for (int s = 1; s <= (9 - number); s++) {
				space += " ";
			}
			
			for (int l = number; l >= 2; l--) {
				left += l;
			}
			
			for (int r = 2; r <= number; r++) {
				right += r;
			}
			
			record = space + left + "1" + right + "\n";
			result = result + record;
			number++;
		}
		System.out.println(result);
	}
	
	// shape 05
	public static void main005(String[] args) {
		/*
			1
			1 2
			1 2 3
			1 2 3 4
			1 2 3 4 5
		*/
		int number = 1;
		while (number <= 5) {
			for (int i = 1; i <= number; i++) {
				System.out.print(i + " ");
			}
			System.out.println();
			number++;
		}
	}
	// shape 04
	public static void main004(String[] args) {
		/*
			* * * * *	i = 5
			  * * * *	i = 4
			    * * *	i = 3
			      * *	i = 2
			        * 	i = 1
		*/
		
		int number = 9;
		while(number >= 1) {
			for (int i = 1; i <= 9- number; i++) {
				System.out.print("  ");
			}
			
			for (int j = 1; j <= number; j++) {
				System.out.print(" *");
			}
			
			System.out.println();
			number--;
		}
	}
	
	// shape 03
	public static void main003(String[] args) {
		/*
					*	i = 1
				  * *	i = 2
				* * *	i = 3
			  * * * *	i = 4
			* * * * *	i = 5
	 	*/
		
		int number = 1;
		while (number <= 5) {
			for (int i = 1; i <= 5 - number; i++) {
				System.out.print("  ");
			}
			
			for (int j = 1; j <= number; j++) {
				System.out.print("* ");
			}
			
			System.out.println();
			number++;
		}
	}
	
	// shape 02
	public static void main002(String[] args) {
		/*
			* * * * * 	i = 5
			* * * *		i = 4
			* * *		i = 3
			* *			i = 2
			* 			i = 1
	 	*/
		int number = 5;
		while(number >= 1) {
			for(int i = 1; i <= number; i++) {
				System.out.print("* ");
			}
			System.out.println();
			number--;
		}
	}
	
	// shape 01
	public static void main001(String[] args) {
		/*
		 	*			i = 1
		 	* *			i = 2
		 	* * *		i = 3
		 	* * * * 	i = 4
		 */
		
		int number = 1;
		while (number <= 6) {
			for (int i = 1; i <= number; i++) {
				System.out.print("* ");
			}
			System.out.println();
			number++;
		}
	}

}
