package chap01.basic;

import java.util.Scanner;

public class p06_calculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numberFirst, numberSecond, result = 0;
		String calculate;
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Number First:");
		numberFirst = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Number Second:");
		numberSecond = scanner.nextInt();
		scanner.nextLine();
		
		System.out.print("Calculate:");
		calculate = scanner.nextLine();
		
		scanner.close();
		
		switch (calculate) {
			case "+":
				result = numberFirst + numberSecond;
				break;
				
			case "-":
				result = numberFirst - numberSecond;
				break;
				
			case "*":
			case "x":
				result = numberFirst * numberSecond;
				break;
				
			case "/":
			case ":":
				result = numberFirst / numberSecond;
				break;
				
			case "%":
				result = numberFirst % numberSecond;
				break;
	
			default:
				result = numberFirst + numberSecond;
				calculate = "+";
				break;
		}
		
		System.out.println("-----------------------------");
		System.out.printf("%d %s %d = %d", numberFirst, calculate, numberSecond, result);
	}	

}
