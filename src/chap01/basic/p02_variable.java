package chap01.basic;

public class p02_variable {
	// 004 Type casting
	public static void main(String[] args) {
		int var1 = 12;
		double var2 = 1.23;
		
		int var3 = var1 + (int)var2;
		double var4 = (double)var1 + var2;
		
		System.out.println(var3);
		System.out.println(var4);
	}
	
	// 003 Data type
	public static void main003(String[] args) {
		int var1 = 12;
		byte var2 = 10;
		boolean var3 = true;
		double var4 = 123.456;
		String name = "John Smith";
		
		System.out.println("var1: " + var1);
		System.out.println("var2: " + var2);
		System.out.println("var3: " + var3);
		System.out.println("var4: " + var4);
		System.out.println("name: " + name);
	}
	
	// 002 Constant
	public static void main002(String[] args) {
		final int YOUR_BIRTHDAY = 1988;
		System.out.println(YOUR_BIRTHDAY);
	}
	
	// 001 Variable
	public static void main001(String[] args) {
		// System.out.println("Variable");
		// Khai báo biến
		// int age, year;
		// char name, city;

		// First name
		// char fistname; // normal
		// char fist_name; // underscore
		// char fist-name; // hyphen java not support
		char fistName; // camelCase

		int age = 21, year = 2015;
		char name = 'n';

		System.out.println("age: " + age);
		
		age = 30;
		
		System.out.println("age: " + age);
//		System.out.print("year: " + year + "\n");
//		System.out.print("name: " + name + "\n");
	}
}
