package chap01.basic;

public class p05_loop {
	// 004 Break continue
	public static void main004(String[] args) {
		for (int i = 1; i<= 20; i++) {
			if (i % 7 == 0) {
				System.out.println("OK: " + i);
				continue;
			}
			
			System.out.println(i);
		}
	}
	
	// 003 Bang cuu chuong full
	public static void main(String[] args) {
		int number = 9;
		
		for (int i = 2; i <= number; i++ ) {
			System.out.println("Bang cuu chuong: " + i);
			
			for (int j = 1; j <= 9; j++) {
				System.out.printf("%d x %d = %d %n", i, j, i*j);
			}
			
			System.out.println();
		}
	}
	
	// 002 Bang cuu chuong Basic
	public static void main002(String[] args) {
		int number = 3;
		for (int i = 1; i <= 10; i++) {
			System.out.printf("%d x %d = %d %n", number, i, number*i);
		}
		
		System.out.println("-------------------------------------");
		
		int j = 1;
		while(j <= 10) {
			System.out.printf("%d x %d = %d %n", number, j, number*j);
			j++;
		}
		
		System.out.println("-------------------------------------");
		
		int k = 1;
		do {
			System.out.printf("%d x %d = %d %n", number, j, number*k);
			k++;
		}while(k <= 10);
	}
	
	// 001 Loop
	public static void main001(String[] args) {
		// TODO Auto-generated method stub
		int number = 8;
		
		// Vong lap For
		for (int i = 1; i < 10; i++) {
			//System.out.println(i + " - Java is not difficult!");
		}
		
		// Vong lap While
		int j = 1;
		while(j <= 10) {
			System.out.println(j + " - Java is not difficult!");
			j++;
		}
		
		// Do while
		int k = 5;
		do {
			System.out.println(k + " - Java is not difficult!");
			k++;
		} while(k < 5);
	}
}
