package chap01.basic;

public class p04_condition {
	// 003 SWITCH
	public static void main(String[] args) {
		int number = 4;
		String result = "";
		
		switch (number) {
		case 1:
			result = "Thứ 2";
			break;
		case 3:
			result = "Thứ 3";
			break;
		default:
			result = "Không hợp lệ";
			break;
		}
		
		System.out.println(result);
	}
	
	// 001 IF
	public static void main001(String[] args) {
		// TODO Auto-generated method stub
		int number = 14;
		int tmp = number % 2;
		if (tmp == 0) {
			System.out.println("Số chẵn");
		} else {
			System.out.println("Số lẻ");
		}	
	}
}
