package chap01.basic;

import java.util.Scanner;

public class p07_find {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int MAX_NUMBER = 100;
		final int MIN_NUMBER = 1;
		int range = (MAX_NUMBER - MIN_NUMBER) + 1;
		int secretNumber = (int)(Math.random() * range) + MIN_NUMBER;
		
		Scanner scanner = new Scanner(System.in);
		int yourNumber = 0;
		int score = 0;
		while (secretNumber != yourNumber) {
			System.out.println("Your number (0 - 100)");
			yourNumber = scanner.nextInt();
			scanner.nextLine();
			
			if (yourNumber > secretNumber) {
				System.out.println("Greathan!");
			} else if (yourNumber < secretNumber) {
				System.out.println("Less than");
			} else {
				System.out.println("Successfull");
			}
			
			score++;
		}
		scanner.close();
		System.out.println("Secret Number:" + secretNumber);
		System.out.println("Score: " + score);
	}

}
