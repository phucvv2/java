package bookstore;

import java.util.Scanner;

public class Main {
	// Khoi tao object book
	private static Book book = null;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int functionId = 0;
		boolean flag = true;

		do {
			// show menu
			showMenu();

			functionId = scanner.nextInt();
			scanner.nextLine();

			switch (functionId) {
			case 1:
				addBook();
				break;
			case 2:
				editBook();
				break;
			case 3:
				infoBook();
				break;
			default:
				flag = false;
				break;
			}

			// Kiem tra functionId;
			//System.out.println(functionId);
		} while (flag == true);

		scanner.close();
	}

	public static void showMenu() {
		System.out.println("=============================Book Manager=============================");
		System.out.println("1.Add book");
		System.out.println("2.Edit book");
		System.out.println("3.Infor book");
		System.out.println("4.Exit");
		System.out.println("Your choice: [1-4]:");
	}

	public static void addBook() {
		Scanner sc = new Scanner(System.in);
		String bookName = "";
		String bookId = "";
		double bookPrice = 0;

		System.out.println("ID: ");
		bookId = sc.nextLine();

		System.out.println("Name: ");
		bookName = sc.nextLine();

		System.out.println("Price: ");
		bookPrice = sc.nextDouble();

		book = new Book(bookId, bookName, bookPrice);

	}

	public static void editBook() {
		if (book != null) {
			Scanner sc = new Scanner(System.in);
			String bookName = "";
			double bookPrice = 0;

			System.out.println("Name: ");
			bookName = sc.nextLine();

			System.out.println("Price: ");
			bookPrice = sc.nextDouble();

			book.setName(bookName);
			book.setPrice(bookPrice);
		} else {
			System.out.println("Book is not exist !");
		}
	}

	public static void infoBook() {
		if (book != null) {
			book.showInfor();
		} else {
			System.out.println("Book is not exist !");
		}
	}
}
